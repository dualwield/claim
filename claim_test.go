package claim

import (
	"testing"
)

func Test_basicClaim(t *testing.T) {

	item := `{"scope":"metatron"}`
	secret := "d59d2849fe57068ec5a1688169cbe3aca00b6e1cdef0c4f45b588660dbe596f8"

	cx := New([]byte(item), []byte(secret))
	if cx.String() != "7b2273636f7065223a226d65746174726f6e227d|8326d0eb48f38bd43ac6f61576b17a342a48642c0c3592764b9ed211213d31e8" {
		t.Fail()
	}
}

func Test_claimValid(t *testing.T) {
	item := `{"scope":"metatron"}`
	secret := "d59d2849fe57068ec5a1688169cbe3aca00b6e1cdef0c4f45b588660dbe596f8"
	cx := New([]byte(item), []byte(secret))

	if !cx.Check([]byte(secret)) {
		t.Fail()
	}
}

func Test_loadClaim(t *testing.T) {
	subject := "7b2273636f7065223a226d65746174726f6e227d|8326d0eb48f38bd43ac6f61576b17a342a48642c0c3592764b9ed211213d31e8"
	claim, err := Load(subject)
	if err != nil {
		t.Error(err)
		return
	}
	if !claim.Check([]byte("d59d2849fe57068ec5a1688169cbe3aca00b6e1cdef0c4f45b588660dbe596f8")) {
		t.Error(claim)
	}

	if string(claim.Data) != `{"scope":"metatron"}` {
		t.Error("claim data not equal")
	}
}
