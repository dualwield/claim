package claim

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/pkg/errors"
)

func New(data []byte, secret []byte) Claim {

	dataHex := hex.EncodeToString(data)
	secretHex := hex.EncodeToString(secret)
	formatHex := fmt.Sprintf("v1 %s %s", dataHex, secretHex)
	hash := sha256.New()
	hash.Write([]byte(formatHex))

	signatureHex := hex.EncodeToString(hash.Sum(nil))

	return Claim{
		Data:      data,
		Signature: signatureHex,
		string:    fmt.Sprintf("%s|%s", dataHex, signatureHex),
	}
}

const formatErrorMsg = "v1 claim must be in format {hex body}|{hex signature}"

func Load(claim string) (Claim, error) {
	split := strings.Split(claim, "|")
	if len(split) != 2 {
		return Claim{}, errors.New(formatErrorMsg)
	}
	data, err := hex.DecodeString(split[0])
	if err != nil {
		return Claim{}, errors.Wrap(err, formatErrorMsg)
	}
	return Claim{
		Data:      data,
		Signature: split[1],
		string:    claim,
	}, nil
}

type Claim struct {
	Data      []byte
	Signature string
	string    string
}

func (c Claim) String() string {
	return c.string
}

func (c Claim) Check(secret []byte) bool {
	new := New(c.Data, secret)
	return new.string == c.string
}
