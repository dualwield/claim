package main

import (
	"fmt"

	"gitlab.com/dualwield/claim"
)

func main() {
	cx := claim.New([]byte(`v1 jab people none 1234`), []byte("a9400b4d887b34b8aa0538ac9137df0a4bdd90d0b1ac124ae9822e7bb9506f5ee53b19e0eabf0e7bf57dd947b0902e956cd4d0e45b6532b0cefd1cec6f37653b"))
	fmt.Println(cx.String())
}
